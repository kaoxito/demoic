package pe.com.interbank.demo.services;

import javax.jws.WebMethod;
import javax.jws.WebService;

import pe.com.interbank.demo.bean.Persona;
import pe.com.interbank.demo.bean.Respuesta;

@WebService
public class PersonaService {

	@WebMethod
	private Respuesta nuevo(Persona persona) {
		Respuesta respuesta = new Respuesta();
		respuesta.setCodigo("1");
		respuesta.setMensaje("OK");
		
		return respuesta;
	}
	
	@WebMethod
	private Respuesta modificar(Persona persona) {
		Respuesta respuesta = new Respuesta();
		respuesta.setCodigo("1");
		respuesta.setMensaje("OK");
		
		return respuesta;
	}
	
	@WebMethod
	private Respuesta eliminar(String dni) {
		Respuesta respuesta = new Respuesta();
		respuesta.setCodigo("1");
		respuesta.setMensaje("OK");
		
		return respuesta;

	}
	
}
